let numero = null;
$(function () {

    // Click boton tirar
    $('.tirar').click(function () {
        // Generar un numero aleatorio entre 1 y 6
        numero = Math.round(Math.random() * (6 - 1) + 1);

        // Dependiendo del numero, a la img de clase dado le ponemos el atributo src con la foto del dado
        switch (numero) {
            case 1:
                $(".dado").attr("src", "imgs/uno.PNG");
                break;
            case 2:
                $(".dado").attr("src", "imgs/dos.PNG");
                break;
            case 3:
                $(".dado").attr("src", "imgs/tres.PNG");
                break;
            case 4:
                $(".dado").attr("src", "imgs/cuatro.PNG");
                break;
            case 5:
                $(".dado").attr("src", "imgs/cinco.PNG");
                break;
            case 6:
                $(".dado").attr("src", "imgs/seis.PNG");
                break;

            default:
                break;
        }

    });

});