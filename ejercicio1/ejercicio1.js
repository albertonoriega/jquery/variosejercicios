let nota1 = 0;
let nota2 = 0;
let nota3 = 0;
let notamedia = 0;

$(function () {

    $('.calcular').click(function () {
        // Guardamos las notas introducidas en la variables
        // ParseInt => covertirlo en números porque son textos
        // val() para obtener el valor del input
        nota1 = parseInt($('#num1').val());
        nota2 = parseInt($('#num2').val());
        nota3 = parseInt($('#num3').val());
        // Calculamos la notamedia
        notamedia = (nota1 + nota2 + nota3) / 3;
        // Colocamos la nota media en el div #resultado
        $('#resultado').text(notamedia.toFixed(2));
        // .toFixed => redondear a dos decimales

        //Cambiamos el color de fondo del div en funcion de la nota

        //Si es mayor o igual que 5
        if ($('#resultado').text() >= 5) {
            $('#resultado').css({
                "backgroundColor": "green"
            })
            //Si es menor que 5
        } else {
            $('#resultado').css({
                "backgroundColor": "red"
            })
        }

    });

    // Borrar el resultado y los datos introducidos
    $('.borrar').click(function () {
        $('#resultado').text("");
        $('#num1').val("");
        $('#num2').val("");
        $('#num3').val("");
    });
});
