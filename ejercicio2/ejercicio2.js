let cadena = [];
let texto = "";
$(function () {

    // Click boton continuar
    $('#continuar').click(function () {
        //Guardamos el valor del texto introducido
        texto = ($('#texto')).val();
        // Introducimos el texto al array con un push
        cadena.push(texto);
        // Borramos el input
        $('#texto').val("");

    });
    // Click botón fin
    $('#fin').click(function () {
        // En el div #resultado imprimimos el array separado por guiones
        $('#resultado').text(cadena.join(" - "));

    });
});