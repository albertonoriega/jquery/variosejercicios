$(function () {

    // Click boton comprobar
    $('#comprobar').click(function () {
        //Guardamos el valor de la ciudad introducida
        ciudad = $('#ciudad').val();

        //Iteramos cada input
        $('.ciudad').each(function (indice, valor) {
            // comprobamos que si la ciudad introducida coincide con un each
            if (valor.value == ciudad) {
                // si coincide cambiamos el input de hidden a text
                $(this).prop("type", "text");
            }
        });

    });

});