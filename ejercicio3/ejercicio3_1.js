$(function () {

    // Click boton comprobar
    $('#comprobar').click(function () {
        //Guardamos el valor de la ciudad introducida
        ciudad = $('#ciudad').val();

        //Iteramos cada div
        $('.ciudad').each(function (indice, valor) {

            // comprobamos que si la ciudad introducida coincide con un each
            if (valor.innerHTML == ciudad) {
                // si coincide cambiamos el div de hidden a visible

                $(this).css("visibility", "visible");
            }
        });

    });

});